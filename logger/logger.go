package logger

import (
	"fmt"
	"log"
	"os"
)

var (
	InfoLogger    *log.Logger
	WarningLogger *log.Logger
	ErrorLogger   *log.Logger
	FatalLogger   *log.Logger
)

func InitStdOutLogger() {
	prepareLogger(os.Stdout)
}

func InitLogger(logfilePath string) error {
	logFile, err := os.OpenFile(logfilePath, os.O_APPEND|os.O_CREATE|os.O_WRONLY, 0666)
	if err != nil {
		return fmt.Errorf("cannot open or create log file %s: %v", logfilePath, err)
	}

	prepareLogger(logFile)
	return nil
}

func prepareLogger(logFile *os.File) {
	InfoLogger = log.New(logFile, "INFO: ", log.Ldate|log.Ltime)
	WarningLogger = log.New(logFile, "WARNING: ", log.Ldate|log.Ltime)
	ErrorLogger = log.New(logFile, "ERROR: ", log.Ldate|log.Ltime)
	FatalLogger = log.New(logFile, "ERROR: ", log.Ldate|log.Ltime)
}

func Info(format string, a ...interface{}) {
	InfoLogger.Println(fmt.Sprintf(format, a...))
}

func Warning(format string, a ...interface{}) {
	WarningLogger.Println(fmt.Sprintf(format, a...))
}

func Error(format string, a ...interface{}) {
	ErrorLogger.Println(fmt.Sprintf(format, a...))
}

func Fatal(format string, a ...interface{}) {
	FatalLogger.Println(fmt.Sprintf(format, a...))
	os.Exit(1)
}
